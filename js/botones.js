document.addEventListener("DOMContentLoaded", function() {
    function menu() {
        window.location.href = '/html/menu.html';
    }

    function productos() {
        window.location.href = '/html/productos.html';
    }

    function articulos() {
        window.location.href = '/html/articulos.html';
    }

    function categorias() {
        window.location.href = '/html/categorias.html';
    }

    function ventas() {
        window.location.href = '/html/ventas.html';
    }

    function compras() {
        window.location.href = '/html/compras.html';
    }

    document.getElementById("menu").addEventListener("click", menu);
    document.getElementById("productos").addEventListener("click", productos);
    document.getElementById("articulos").addEventListener("click", articulos);
    document.getElementById("categorias").addEventListener("click", categorias);
    document.getElementById("ventas").addEventListener("click", ventas);
    document.getElementById("compras").addEventListener("click", compras);
    
});